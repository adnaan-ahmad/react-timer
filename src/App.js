import React from 'react'

export default class App extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      hours: 0,
      minutes: 0,
      seconds: 0
    }
  }

  componentDidUpdate() {

    if (this.state.seconds === 59 && this.state.minutes < 59) {
      this.setState({ minutes: this.state.minutes + 1, seconds: 0 })
    }
    if (this.state.minutes === 59) {
      this.setState({ hours: this.state.hours + 1, seconds: 0, minutes: 0 })
    }
  }

  start = () => {
    this.interval = setInterval(
      () => this.setState((prevState) => ({ seconds: prevState.seconds + 1 })),
      1000
    )
  }

  pause = () => {
    clearInterval(this.interval)
  }

  reset = () => {
    this.setState({
      hours: 0,
      minutes: 0,
      seconds: 0
    }, () => clearInterval(this.interval))
  }

  render() {
    return (
      <div style={{ flex: 1, justifyContent: 'center', alignItems: 'center', display: 'flex', flexDirection: 'column' }}>
        <p>
          {this.state.hours} : {this.state.minutes} : {this.state.seconds}
        </p>

        <div style={{ cursor: 'pointer' }} onClick={() => this.start()}>
          <p>Start</p>
        </div>
        <div style={{ cursor: 'pointer' }} onClick={() => this.pause()}>
          <p>Pause</p>
        </div>
        <div style={{ cursor: 'pointer' }} onClick={() => this.reset()}>
          <p>Reset</p>
        </div>
      </div>
    )
  }
}
